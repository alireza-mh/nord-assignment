import React from "react";
import styles from "./style.module.scss";

interface ILoading {
  className?: string;
}
const Loading: React.FC<ILoading> = (props) => {
  const { className = "" } = props;
  return (
    <div className={`${styles["loading__wrapper"]} ${className}`}>
      <div className={`${styles["loading__spinner"]}`} />
      <span>Loading...</span>
    </div>
  );
};

export default Loading;
