import React from "react";
import { useIsMobile } from "src/hooks";
import NavMenuDesktop from "src/components/NavMenuView/NavMenuDesktop";
import NavMenuMobile from "src/components/NavMenuView/NavMenuMobile";

const NavMenuView: React.FC<INavMenuProps> = (props) => {
  const isMobile = useIsMobile();

  if (isMobile) {
    return <NavMenuMobile {...props} />;
  }

  return <NavMenuDesktop {...props} />;
};

export default NavMenuView;
