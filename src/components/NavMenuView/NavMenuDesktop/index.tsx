import React from "react";
import { Link } from "react-router-dom";
import styles from "./style.module.scss";

interface INavMenuDesktopProps {
  className?: string;
  menuItems: MenuType[];
}
const NavMenuDesktop: React.FC<INavMenuDesktopProps> = (props) => {
  const { className = "", menuItems } = props;
  return (
    <nav className={`${styles["nav-menu-view-desktop"]} ${className}`}>
      <ul className={styles["nav-menu-view-desktop__list"]}>
        {menuItems.map((menuItem) => (
          <li
            key={menuItem.label}
            className={styles["nav-menu-view__list-item"]}
          >
            <Link
              to={menuItem.path || "#"}
              state={menuItem.state}
              onClick={menuItem.onClick}
            >
              {menuItem.label}
            </Link>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default NavMenuDesktop;
