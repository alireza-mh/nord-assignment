type MenuType = {
  label: string;
  path?: string;
  state?: string;
  onClick?: (e?: any) => void;
};
interface INavMenuProps {
  className?: string;
  menuItems: MenuType[];
}
