import React, { useState } from "react";
import { ReactComponent as BurgerMenuIcon } from "../assets/images/icon-hamburger.svg";
import styles from "./style.module.scss";
import { Link } from "react-router-dom";

interface INavMenuMobileProps {
  className?: string;
  menuItems: MenuType[];
}
const NavMenuMobile: React.FC<INavMenuMobileProps> = (props) => {
  const { className = "", menuItems } = props;
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  /**
   * @method toggleMenu
   * @description Toggle menu open/close
   */
  const toggleMenu = () => {
    setIsMenuOpen(!isMenuOpen);
  };

  return (
    <>
      <nav className={`${styles["nav-menu-view-mobile__icon"]} ${className}`}>
        <BurgerMenuIcon onClick={toggleMenu} />
      </nav>
      <nav
        className={`${styles["nav-menu-view-mobile__menu"]}  ${
          isMenuOpen ? styles["nav-menu-view-mobile__menu_open"] : ""
        }`}
      >
        <ul className={styles["nav-menu-view-mobile__list"]}>
          {menuItems.map((menuItem) => (
            <li key={menuItem.label} onClick={toggleMenu}>
              <Link
                to={menuItem.path || "#"}
                state={menuItem.state}
                onClick={menuItem.onClick}
                className={styles["nav-menu-view__list-item"]}
              >
                {menuItem.label}
              </Link>
            </li>
          ))}
        </ul>
      </nav>
    </>
  );
};

export default NavMenuMobile;
