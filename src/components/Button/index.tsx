import React from "react";
import styles from "./style.module.scss";

interface IButton extends React.ButtonHTMLAttributes<HTMLButtonElement> {}
const Button: React.FC<IButton> = (props) => {
  const { className = "", children, ...rest } = props;
  return (
    <button className={`${styles["button"]} ${className}`} {...rest}>
      {children}
    </button>
  );
};

export default Button;
