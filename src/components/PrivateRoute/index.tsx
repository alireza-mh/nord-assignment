import React, { Fragment } from "react";
import { Navigate, Outlet } from "react-router-dom";
import useIsAuthenticate from "src/hooks/useIsAuthenticate";
import { RouteProps } from "react-router/dist/lib/components";

/**
 * @param props: IPrivateRouteProps
 */

type IPrivateRouteProps = RouteProps;
const PrivateRoute: React.FC<IPrivateRouteProps> = (
  props: IPrivateRouteProps
) => {
  const { isAuthenticate } = useIsAuthenticate();
  return (
    <Fragment>
      {isAuthenticate && <Outlet />}
      {isAuthenticate === false && <Navigate to={"/login"} />}
    </Fragment>
  );
};

export default PrivateRoute;
