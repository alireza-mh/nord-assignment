import React from "react";
import styles from "./style.module.scss";

interface ITextInput extends React.InputHTMLAttributes<HTMLInputElement> {
  label: string;
  id: string;
}
const TextInput: React.FC<ITextInput> = (props) => {
  const { className = "", label, id, ...rest } = props;
  return (
    <div className={`${styles["text-input-wrapper"]} ${className}`}>
      <label htmlFor={id}>{label}</label>
      <input id={id} className={`${styles["text-input"]}`} {...rest} />
    </div>
  );
};

export default TextInput;
