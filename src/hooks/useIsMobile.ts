import { useEffect, useState } from "react";

const MOBILE_BREAKPOINT = 768 as const;
const getIsMobile = () => window.innerWidth <= MOBILE_BREAKPOINT;
const useIsMobile = () => {
  const [isMobile, setIsMobile] = useState(getIsMobile());

  useEffect(() => {
    const onResize = () => {
      setIsMobile(getIsMobile());
    };

    window.addEventListener("resize", onResize);

    return () => {
      window.removeEventListener("resize", onResize);
    };
  }, []);

  return isMobile;
};

export default useIsMobile;
