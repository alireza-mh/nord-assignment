import { useQuery } from "@tanstack/react-query";
import { axiosAPI, setAuthToken } from "src/api";
import { useSelector } from "react-redux";
import { getAuthTokenSelector } from "src/store/selector";
import { store } from "src/store";

const useIsAuthenticate = () => {
  const storageToken = useSelector(getAuthTokenSelector);
  storageToken && setAuthToken(storageToken);

  const { isLoading, data, isError } = useQuery({
    queryKey: ["isAuthenticate"],
    queryFn: () => axiosAPI.get("/servers"),
    enabled: Boolean(storageToken),
    refetchOnWindowFocus: false,
    retry: false,
  });

  if (isError || storageToken === "") {
    return { isLoading: false, isAuthenticate: false };
  }

  if (data) {
    return { isLoading: false, isAuthenticate: true };
  }

  return { isLoading, isAuthenticate: null };
};

export default useIsAuthenticate;
