import React from "react";
import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Login from "src/routes/Login";
import Providers from "src/providers";
import * as process from "process";
import { setBaseURL } from "src/api";

const renderProvider = (children: React.ReactNode) =>
  render(<Providers>{children}</Providers>);

describe("Login Page", () => {
  test("Check inputs existence", () => {
    renderProvider(<Login />);
    const usernameInput = screen.getByLabelText(/username/i);
    expect(usernameInput).toBeInTheDocument();
    const passwordInput = screen.getByLabelText(/password/i);
    expect(passwordInput).toBeInTheDocument();
  });

  test("Button be disable in initial form load", () => {
    renderProvider(<Login />);
    const button = screen.getByRole("button", { name: /login/i });
    expect(button).toBeDisabled();
  });

  test("Button should not be disabled", async () => {
    renderProvider(<Login />);
    const usernameInput = screen.getByLabelText(/username/i);
    const passwordInput = screen.getByLabelText(/password/i);
    await userEvent.type(usernameInput, "test");
    await userEvent.type(passwordInput, "test");
    const button = screen.getByRole("button", { name: /login/i });
    expect(button).not.toBeDisabled();
  });

  test("Show signin failed toast", async () => {
    renderProvider(<Login />);
    const usernameInput = screen.getByLabelText(/username/i);
    const passwordInput = screen.getByLabelText(/password/i);
    await userEvent.type(usernameInput, "test");
    await userEvent.type(passwordInput, "test");
    const button = screen.getByRole("button", { name: /login/i });
    await userEvent.click(button);
    const toast = await waitFor(() =>
      screen.getByText(/wrong username or password/i)
    );
    expect(toast).toBeInTheDocument();
  });
});
