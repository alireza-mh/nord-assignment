import React, { useRef, useState } from "react";
import TextInput from "src/components/TextInput";
import Button from "src/components/Button";
import { useLoginAPIMutate } from "src/api/apiHooks";
import { setAuthToken } from "src/api";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setAuthTokenStore } from "src/store/ducks/AuthToken";
import { toast } from "react-toastify";
import styles from "./style.module.scss";

const Login: React.FC = () => {
  const { mutate: loginMutate, isLoading: isLoginLoading } =
    useLoginAPIMutate();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [formState, setFormState] = useState({ username: "", password: "" });
  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    loginMutate(
      { username: formState.username, password: formState.password },
      {
        onSuccess: ({ data }) => {
          setAuthToken(data.token);
          dispatch(setAuthTokenStore(data.token));
          toast("Signed in", { type: "success" });
          navigate("/");
        },
        onError: () => {
          toast("Wrong username or password", { type: "error" });
        },
      }
    );
  };

  return (
    <div data-testid="login" className={styles["login"]}>
      <form className={styles["login-form"]} onSubmit={handleSubmit}>
        <TextInput
          label="Username"
          id="username"
          name="username"
          placeholder="Please enter your username"
          autoComplete="username"
          className={styles["login-form__input"]}
          onChange={(e) =>
            setFormState({ ...formState, username: e.target.value })
          }
        />
        <TextInput
          label="Password"
          id="password"
          name="password"
          type="password"
          placeholder="Please enter your password"
          autoComplete="password"
          className={styles["login-form__input"]}
          onChange={(e) =>
            setFormState({ ...formState, password: e.target.value })
          }
        />
        <Button
          type="submit"
          disabled={
            isLoginLoading || !formState.password || !formState.username
          }
        >
          Login
        </Button>
      </form>
    </div>
  );
};

export default Login;
