import React, { useEffect, useMemo, useState } from "react";
import Table from "src/components/Table";
import { useQuery } from "@tanstack/react-query";
import { axiosAPI } from "src/api";
import { ColumnDef } from "@tanstack/react-table";
import { ReactComponent as SortIcon } from "./assets/images/icon-sort.svg";
import Loading from "src/components/Loading";
import styles from "./style.module.scss";

interface IHeaderTableProps {
  className?: string;
  label: string;
  handleClick: (name: ServerColType) => void;
  name: ServerColType;
  sortCol: SortColType;
}
const HeaderTable: React.FC<IHeaderTableProps> = (props) => {
  const { className = "", label, handleClick, name, sortCol } = props;
  return (
    <span className={`${styles["servers-table-header"]} ${className}`}>
      {label}{" "}
      <SortIcon
        className={
          sortCol.colName === name
            ? styles[`sort-icon_${sortCol.direction}`]
            : ""
        }
        onClick={() => handleClick(name)}
      />
    </span>
  );
};
const Servers: React.FC = () => {
  const [serverData, setServerData] = useState<ServerResponseType[]>([]);
  const { isLoading, data: neutralServerData } = useQuery({
    queryKey: ["servers"],
    queryFn: () => axiosAPI.get("/servers"),
    select: ({ data }) => data,
    onSuccess: (data) => {
      setServerData(data);
    },
  });

  const [sortColumn, setSortColumn] = useState<SortColType>({
    colName: "name",
    direction: null,
  });

  const handleSort = (column: ServerColType) => {
    if (sortColumn.colName === column) {
      if (!sortColumn.direction) {
        setSortColumn({ colName: column, direction: "asc" });
      } else if (sortColumn.direction === "asc") {
        setSortColumn({ colName: column, direction: "desc" });
      } else {
        setSortColumn({ colName: column, direction: null });
      }
    } else {
      setSortColumn({ colName: column, direction: "asc" });
    }
  };

  useEffect(() => {
    if (!isLoading && sortColumn.direction) {
      const sortedData = [...serverData].sort((a, b) => {
        if (sortColumn.direction === "asc") {
          return a[sortColumn.colName] > b[sortColumn.colName] ? 1 : -1;
        } else {
          return a[sortColumn.colName] < b[sortColumn.colName] ? 1 : -1;
        }
      });
      setServerData(sortedData);
    } else {
      setServerData(neutralServerData);
    }
  }, [sortColumn]);

  const columns: ColumnDef<any, any>[] = useMemo(
    () => [
      {
        header: () => (
          <HeaderTable
            name="name"
            label="Name"
            handleClick={handleSort}
            sortCol={sortColumn}
          />
        ),
        accessorKey: "name",
      },
      {
        header: () => (
          <HeaderTable
            name="distance"
            label="Distance"
            handleClick={handleSort}
            sortCol={sortColumn}
          />
        ),
        accessorKey: "distance",
      },
    ],
    [sortColumn]
  );

  if (isLoading) {
    return <Loading className={styles["servers-loading"]} />;
  }

  return (
    <div className={styles["servers"]}>
      <Table columns={columns} data={serverData || []} />
    </div>
  );
};

export default Servers;
