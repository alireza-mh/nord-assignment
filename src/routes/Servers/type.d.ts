type ServerColType = "name" | "distance";

type SortColType = {
  colName: ServerColType;
  direction: "asc" | "desc" | null;
};

type ServerResponseType = {
  name: string;
  distance: number;
};
