import { Navigate, Route, Routes } from "react-router-dom";
import Layout from "src/containers/Layout";
import Login from "src/routes/Login";
import Servers from "src/routes/Servers";
import PrivateRoute from "src/components/PrivateRoute";

const RouteManagement = () => {
  return (
    <Routes>
      <Route path="/" element={<Layout />}>
        <Route path="/" element={null} />
        <Route path="/login" element={<Login />} />
        <Route element={<PrivateRoute />}>
          <Route path="/servers" element={<Servers />} />
        </Route>
      </Route>
      <Route path="*" element={<Navigate to="/" />} />
    </Routes>
  );
};

export default RouteManagement;
