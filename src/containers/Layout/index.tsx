import React from "react";
import { Outlet } from "react-router-dom";
import Header from "src/containers/Layout/Header";
import styles from "./style.module.scss";

const Layout: React.FC = () => {
  return (
    <div className={styles["layout__wrapper"]}>
      <Header />
      <div className={styles["layout__content"]}>
        <Outlet />
      </div>
    </div>
  );
};

export default Layout;
