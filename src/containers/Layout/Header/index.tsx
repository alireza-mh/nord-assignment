import React from "react";
import NavMenu from "./Component/NavMenu";
import { ReactComponent as Logo } from "../assets/images/TesoServers.svg";
import styles from "./style.module.scss";

interface IHeaderProps {
  className?: string;
}

const Header: React.FC<IHeaderProps> = (props) => {
  const { className = "" } = props;
  return (
    <header className={`${styles["header"]} ${className}`}>
      <div className={styles["header__logo"]}>
        <Logo />
      </div>
      <NavMenu />
    </header>
  );
};

export default Header;
