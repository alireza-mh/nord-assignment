import NavMenuView from "src/components/NavMenuView";
import useIsAuthenticate from "src/hooks/useIsAuthenticate";
import { logout } from "src/utils";

const CommonMenu: MenuType[] = [
  {
    label: "Main",
    path: "/",
  },
];

const PublicMenu = [
  {
    label: "Login",
    path: "/login",
  },
];

const PrivateMenu = [
  { label: "Servers", path: "/servers" },
  { label: "Logout", onClick: logout },
];

interface INavMenuProps {
  className?: string;
}
const NavMenu: React.FC<INavMenuProps> = (props) => {
  const { className = "" } = props;
  const { isAuthenticate } = useIsAuthenticate();
  const menuItems = isAuthenticate
    ? [...CommonMenu, ...PrivateMenu]
    : [...CommonMenu, ...PublicMenu];
  return <NavMenuView className={className} menuItems={menuItems} />;
};

export default NavMenu;
