import { useMutation, UseMutationOptions } from "@tanstack/react-query";
import { axiosAPI } from "src/api/index";
import { AxiosError } from "axios";

export const useLoginAPIMutate = (
  options?: UseMutationOptions<
    Awaited<ReturnType<typeof axiosAPI.post>>,
    AxiosError,
    { username: string; password: string },
    unknown
  >
) => {
  return useMutation<
    any,
    AxiosError,
    { username: string; password: string },
    unknown
  >(["login"], (data) => axiosAPI.post("/tokens", data), options);
};
