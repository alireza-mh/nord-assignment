import { BASE_URL_API } from "src/constants";
import Axios from "axios";

export const axiosAPI = Axios.create({
  baseURL: BASE_URL_API,
  headers: { contentType: "application/json" },
}); // use your own URL here or environment variable

export const setBaseURL = (baseURL: string) => {
  axiosAPI.defaults.baseURL = baseURL;
};

export const setAuthToken = (token: string) => {
  axiosAPI.defaults.headers.common.Authorization = `Bearer ${token}`;
};

export const removeAuthToken = () => {
  axiosAPI.defaults.headers.common.Authorization = undefined;
};
