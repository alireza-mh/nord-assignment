import { RootState } from "src/store/ducks/reducer";

export const getAuthTokenSelector = (state: RootState) => state.AuthToken;
