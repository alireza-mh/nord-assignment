import configStore from "./configStore";
import { Store } from "redux";
import { Persistor } from "redux-persist/es/types";

type ReduxStoreType = { store: Store; persistor: Persistor };
const index: ReduxStoreType = configStore();

export const persistor = index.persistor;
export const store = index.store;
export default index;
