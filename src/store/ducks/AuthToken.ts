// Actions
export const SET_AUTH_TOKEN = "AUTH_TOKEN/SET";
export const RESET_AUTH_TOKEN = "AUTH_TOKEN/RESET";

interface IAction<T> {
  payload?: T;
  type: string;
}

// Reducer

export default (state: string = "", action: IAction<string>): string => {
  switch (action.type) {
    case SET_AUTH_TOKEN:
      return action.payload || "";
    case RESET_AUTH_TOKEN:
      return "";
    default:
      return state;
  }
};

// Action Creators
export const setAuthTokenStore = (payload: string = "") => {
  return {
    type: SET_AUTH_TOKEN,
    payload,
  };
};

export const resetAuthTokenStore = () => {
  return {
    type: RESET_AUTH_TOKEN,
  };
};
