import { createStore } from "redux";
import rootReducers from "src/store/ducks/reducer";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage"; // defaults to localStorage for web
import { devToolsEnhancer } from "@redux-devtools/extension";

const persistConfig = {
  key: "root",
  storage: storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducers);
const configStore = () => {
  const store = createStore(persistedReducer, devToolsEnhancer({}));
  const persistor = persistStore(store);
  return { store, persistor };
};

export default configStore;
