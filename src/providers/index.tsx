import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { store, persistor } from "src/store";
import { ToastContainer } from "react-toastify";
import "react-toastify/ReactToastify.min.css";

const queryClient = new QueryClient();

interface IProviders {
  children: React.ReactNode;
}
const Providers: React.FC<IProviders> = ({ children }) => {
  return (
    <QueryClientProvider client={queryClient}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <BrowserRouter>{children}</BrowserRouter>
          <ToastContainer position="bottom-left" />
        </PersistGate>
      </Provider>
    </QueryClientProvider>
  );
};

export default Providers;
