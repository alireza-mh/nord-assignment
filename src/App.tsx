import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import RouteManagement from "src/routes";
import Providers from "src/providers";
import "./theme/index.scss";

function App() {
  return (
    <Providers>
      <RouteManagement />
    </Providers>
  );
}

export default App;
