import { removeAuthToken } from "src/api";
import { store } from "src/store";
import { resetAuthTokenStore } from "src/store/ducks/AuthToken";
import { toast } from "react-toastify";

const logout = () => {
  store.dispatch(resetAuthTokenStore());
  removeAuthToken();
  toast("You have been logged out.", { type: "info" });
};

export default logout;
