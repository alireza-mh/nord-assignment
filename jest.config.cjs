module.exports = {
  preset: "ts-jest",
  moduleFileExtensions: ["ts", "tsx", "js"],
  setupFilesAfterEnv: ["./src/setupTests.ts"],
  moduleNameMapper: {
    "\\.(css|less|scss)$": "identity-obj-proxy",
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$":
      "<rootDir>/src/__mocks__/fileMock.ts",
    "\\.svg$": "<rootDir>src/__mocks__/svg.ts",
    "^lodash-es$": "lodash",
    "^src(.*)$": "<rootDir>/src$1",
  },
  transform: {
    "^.+\\.(j|t)sx?$": ["ts-jest"],
  },
  testEnvironment: "jsdom",
};
