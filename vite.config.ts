import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import svgr from "vite-plugin-svgr";
import tsconfigPaths from "vite-tsconfig-paths";
import EnvironmentPlugin from "vite-plugin-environment";
import dns from "dns";

dns.setDefaultResultOrder("verbatim");

// https://vitejs.dev/config/
export default defineConfig(({ command }) => ({
  server: {
    host: "localhost",
    port: 3000,
    proxy: {
      "/v1": {
        changeOrigin: true,
        secure: false,
        target: "https://playground.tesonet.lt",
      },
    },
  },
  plugins: [tsconfigPaths(), svgr(), react(), EnvironmentPlugin("all")],
}));
