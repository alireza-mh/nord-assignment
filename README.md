### NordLocker frontend assignment 

#### Introduction
The project is written in `React` with `TypeScript` and `scss` for styling. In this project we used [vite](https://vitejs.dev/guide/) as build tools for better developer experience and add testing configuration and setups afterward. `yarn` has been used as package manager. There are few integration test has been written that you can run with `yarn test` command.

#### How to run
You need to change `.env.sample` to `.env.local` and for production build you need to change `.env.sample` to `.env` and insert follow value:

```
VITE_BASE_URL_API=https://playground.tesonet.lt/v1/
```

Then you can run `yarn` to install dependencies and `yarn dev` to run the project in development mode. You can also run `yarn build` to build the project for production.
Please run `yarn` to install dependencies and `yarn dev` to start the development server. You should be able to see the project running on `http://localhost:3000`. You can also run all the tests with `yarn test` command.


#### How to build
You can run `yarn build` to build the project. The build will be in `dist` folder.

#### Some notes
There is a proxy configuration in Vite to avoid CORS errors on local development. husky pre-commit also added for static testing and format codes before create a PR. Jest configuration done manually, and it's not part of the boilerplate. Same goes for aliasing paths and SVGR plugin to support SVG as React components.   
